<?php

/**
 * Twittalicious API functions
 */
module_load_include('lib.php', 'twittalicious');

/**
 * Number of tweets function
 * @param $user - the user screen_name
 * @return number of tweets of the given user
 */
function twittalicious_statuses_count($user) {
  $twittalicious = new Twittalicious();

  $statuses = $twittalicious->statuses_count($user);

  return $statuses;
}

/**
 * Number of retweets function
 * @param $user - the user screen_name
 * @return number of retweets received by the given user
 */
function twittalicious_retweets_count($user) {
  $twittalicious = new Twittalicious();

  $retweets = $twittalicious->retweets_count($user);

  return $retweets;
}

/**
 * Number of followers function
 * @param $user - the user screen_name
 * @return number of followers of the given user
 */
function twittalicious_followers_count($user) {
  $twittalicious = new Twittalicious();

  $followers = $twittalicious->followers_count($user);

  return $followers;
}

/**
 * Number of friends function
 * @param $user - the user screen_name
 * @return number of friends of the given user
 */
function twittalicious_friends_count($user) {
  $twittalicious = new Twittalicious();

  $friends = $twittalicious->friends_count($user);

  return $friends;
}

/**
 * Number of lists function
 * @param $user - the user screen_name
 * @return number of lists created by the given user
 */
function twittalicious_lists_count($user) {
  $twittalicious = new Twittalicious();

  $lists = $twittalicious->lists_count($user);

  return $lists;
}

/**
 * Number of lists where the user appears function
 * @param $user - the user screen_name
 * @return number of lists where the given user appears
 */
function twittalicious_listed_count($user) {
  $twittalicious = new Twittalicious();

  $listed = $twittalicious->listed_count($user);

  return $listed;
}

/**
 * Number of tweets favourited by the given user
 * @param $user - the user screen_name
 * @return number of tweets favourited by the given user
 */
function twittalicious_favourites_count($user) {
  $twittalicious = new Twittalicious();

  $favourites = $twittalicious->favourites_count($user);

  return $favourites;
}

/**
 * Returns twitter user info
 * @param $user - the user screen_name or twitter id
 * @return user info object
 */
function twittalicious_user_info($user) {
  $twittalicious = new Twittalicious();

  $user_info = $twittalicious->user_info($user);

  return $user_info;
}

/**
 * Channels list function
 * @returns $channels - array of channel names and info
 */
function twittalicious_channels_list() {
  $channels = array(
    'twitter' => array(
      'title' => 'Twitter',
    ),
  );

  return $channels;
}

/**
 * Creates a chart in JSON format
 * @param $name - the name of the chart you want to create
 * @returns - the chart in JSON format
 */
function _twittalicious_chart_json($name) {
  module_load_include('charts.inc', 'twittalicious');

  $title = ucfirst($name) . ' Chart';
  $container = $name . '-container';

  $chart = _twittalicious_charts_demo($title, $container);

  $chart->chart->width = 320;
  $chart->chart->height = 410;
  $chart->exporting->enabled = false;

  return (object)array(
    'chart' => $chart,
  );
}

/**
 * Returns the twitter user accounts linked to any active drupal users
 * @returns $users - array of twitter user names
 */
function twittalicious_twitter_users() {

  $query = db_select('twitter_account', 'ta');
  $query->join('users', 'u', 'ta.uid = u.uid');
  $query->fields('ta', array('twitter_uid', 'oauth_token', 'oauth_token_secret'));
  $query->fields('u', array('name', 'pass'));
  $users = $query->execute();

  return $users;
}

/**
 * Saves the statistic data of a given drupal user with a twitter account
 */
function twittalicious_save_data($user) {
  $account = twitter_account_load($user->twitter_uid);
  
print_r($twitter_user);
die();
  //$twittalicious = twittalicious_connect($account);

  $twittalicious->set_auth($user->name, $user->pass);

print_r($twittalicious);
die();
 
  $user_data = $twittalicious->users_data($user->twitter_uid, TRUE);

  $data = array();
  $data['id'] = $user;
  $data['screen_name'] = $user_data->screen_name;
  $data['statuses'] = $user_data->statuses_count;
  $data['retweets'] = 0;
  $data['followers'] = $user_data->followers_count;
  $data['friends'] = $user_data->friends_count;
  $data['lists'] = 0;
  $data['listed'] = $user_data->listed_count;;
  $data['favourites'] = $user_data->favourites_count;
  
  print_r($data);
}

