<?php
/**
 * @file twittalicious.lib.php
 * Classes to implement the full Twittalicious API
 */

/**
 * Class Twittalicious
 * Extends the Twitter Class with useful features
 */
require_once(libraries_get_path('twitter') . '/twitter.lib.php');

class Twittalicious extends Twitter{

  /**
   *
   * @see http://apiwiki.twitter.com/Twitter-REST-API-Method%3A-users%C2%A0show
   */
  public function users_data($id, $use_auth = TRUE) {
    $params = array();
    if (is_numeric($id)) {
      $params['user_id'] = $id;
    }
    else {
      $params['screen_name'] = $id;
    }

    $user_data = $this->call('users/show', $params, 'GET', $use_auth);

    return $user_data;
  }

  /**
   * Returns the number of retweets of the user
   *
   * @param $id
   *   user id or screen name
   * @return
   *   number of retweets of the user
   * @TODO implement the retweets count function
   */
  public function retweets_count($id) {
    $result = $this->user_info($id);

    return 0;
  }

  /**
   * Returns the number of lists created by a user
   *
   * @param $id
   *   user id or screen name
   * @param $params
   *   array of parameters for the api call
   * @param $use_auth
   *   sets if the call is authenticated or not
   * @return
   *   number of lists created by the given user
   * @see https://dev.twitter.com/docs/api/1/get/lists
   */
  public function lists_count($id, $params = array(), $use_auth = FALSE) {
    if (!is_numeric($id)) {
      $params['user'] = $id;
    }

    $lists = $this->call('lists', $params, 'GET', $use_auth);

    return sizeof($lists['lists']);
  }

}

